<?php
$info = array(
	'title' => 'ProcessWire Kickstart Recipe Editor', 
	'summary' => 'Module to easily create and test Kickstart Recipes', 
	'version' => 1, 
	'author' => 'Bernhard Baumrock, baumrock.com', 
	'icon' => 'cogs', 
	'page' => array(
		'name' => 'kickstartrecipes',
		'parent' => 'setup', 
		'title' => 'Kickstart Recipes'
	),
	'requires' => 'InputfieldAceExtended',
);
